#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Jun 2019
Based on a demo by Slava Korolev:
    https://towardsdatascience.com/today-im-going-to-talk-about-a-small-practical-example-of-using-neural-networks-training-one-to-6b2cbd6efdb3
"""

from snake_game import SnakeGame
import pygame
from pygame.locals import K_RIGHT,K_LEFT,K_UP,K_DOWN,K_ESCAPE
import time

class GameUI:
    BLOCK_SIZE = 30        # Width of each cell on the board, in pixels
    IMAGES = ['head','tail','apple']

    KEY_MAP={K_UP:0,        # Map of keys to snake game directions
             K_RIGHT:1,
             K_DOWN:2,
             K_LEFT:3,
             }
    SPEED = 5      # frame rate and snake speed (5 -> 5 fps aprox.)
    
    def __init__(self, width = 20, height = 20):
        "setup game with UI"           
        self.game = SnakeGame( width, height)        
        self.windowWidth = width * self.BLOCK_SIZE
        self.windowHeight = height * self.BLOCK_SIZE
        self.start_ui()
        self.last_dir = -1  #snake direction (unknown)


    def start_ui(self):
        #pygame.init()   # for now dont't init !!
        self.display = pygame.display.set_mode(
                (self.windowWidth,self.windowHeight))
        self.images = {}        # load image files
        for img in self.IMAGES:
            tmp = pygame.image.load(img+".png").convert()
            self.images[img] = pygame.transform.scale(tmp, 
                       (self.BLOCK_SIZE, self.BLOCK_SIZE))

    def draw_block(self, image, coord):
        "Draw selected image at the specified coordinates"
        x,y = coord
        x = x*self.BLOCK_SIZE
        y = y*self.BLOCK_SIZE
        self.display.blit(self.images[image],(x,y) ) 

    def render(self):
        "Render the current game state on the game window"
        self.display.fill( (0,0,0) ) # refill with black color
        score,apple,head,tail = self.game.get_state()
        self.draw_block('apple', apple)
        self.draw_block('head', head)
        for t in tail:
            self.draw_block('tail', t)
        pygame.display.set_caption('Snake - Score: '+str(self.game.score))
        pygame.display.flip()       # update window
        
    def test_step(self,action):
        "to test UI: move the snake and show the game state"
        self.game.step(action)
        self.render()

    def loop_step(self):
        "single step in the game loop, checking the keys pressed"
        for event in pygame.event.get():    # handle all events
            if event.type == pygame.KEYDOWN:
                if event.key in self.KEY_MAP:
                    self.last_dir = self.KEY_MAP[event.key]
                elif event.key == K_ESCAPE:
                    self.game.done = True
            elif event.type == pygame.QUIT: # handle window close event
                self.game.done = True
            # ignore other events


    def execute(self):
        "Game main loop"
        self.render()
        while not self.game.done:
            time.sleep( 1.0 /self.SPEED )
            self.loop_step()
            if self.last_dir != -1:     # starts after first key press
                self.game.step(self.last_dir)
                self.render()
        
        pygame.quit()
        print('Game over! Final score: ', self.game.score)


# runs this only if this is the main file
if __name__ == "__main__" :
    uigame = GameUI()
    uigame.execute()
